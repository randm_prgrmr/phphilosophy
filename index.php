<?php 
#################### SETUP / CONFIG #####################
//load framework, templating engine, and ORM
require 'startup.php';
$app->config('debug', true);

//connect to database
R::setup('mysql:host=localhost; dbname=testdb','root','');

require 'Code/Accounts.php';//user & admin login/logout, acct-type-specific headers
require 'Code/ItemManager.php';//admin add/update/remove items, mark items as sold
require 'Code/Cart.php';//shopping cart management, checkout, payment processing
require 'Code/Orders.php';//order processing, tracking, etc.
require 'Code/Search.php';//product search, listings, retrieve next page results

######################### URL ROUTES #########################

//************* User-Facing Pages **************
//home page / storefront
$app->get('/', function() use($app){
    $template_variables = newestItems(); //Search.php
    $app->render('home.html', $template_variables);
});

//checkout / payment processing
$app->get('/checkout',function() use($app){
    //check if shopping cart empty, if so redirect home
    if(!count($_SESSION['cart'])){
        $app->redirect('/');
    }
    $header = getHeader();//Accounts.php
    $template_variables = array('header'=>$header);
    $app->render('checkout.html',$template_variables);
});    

//*************** Form/Button Handling Pages *************
//checkout submit / process transaction / create order
$app->post('/checkout',function() use($app){
    $template_vars = post_Order(); //Orders.php
    $app->render('receipt.html', $template_vars);
});

//login form submit
$app->post('/login', function() use($app){
    $template_vars = post_login(); //Accounts.php
    if($template_vars['error']){
        $app->render('error.html', $template_vars);
    }
    else{
        $app->redirect('/');
    }
});

//logout
$app->get('/logout', function() use($app){
    signOut(); //Accounts.php
    $app->redirect('/');
});

//create new account
$app->post('/newacct', function() use($app){
    $template_vars = post_newAcct(); //Accounts.php
    if($template_vars['error']){
        $app->render('error.html', $template_vars);
    }
    else{
        $app->redirect('/');
    }
});

//fetch more items (for next page, can be used for infinite scroll or traditional pagination)
$app->get('/moreitems', function() use($app){
    $template_variables = moreItems(4); //Search.php
    $app->render('moreitems.html', $template_variables);
});

//****************** SHOPPING CART ********************
//add item to shopping cart (ajax)
$app->get('/addtocart', function() use($app){
    $template_variables = addToCart($_GET['productid']);//Cart.php
    $app->render('dropdowncart.html', $template_variables);
});
//remove item from shopping cart (ajax)
$app->get('/removefromcart', function() use($app){
    $template_variables = removeFromCart($_GET['productid']);//Cart.php
    $app->render('error',$template_variables);
});

//get array of productid's to populate clientside shopping cart
$app->get('/cart.json', function() use($app){
    cartJSON();//Cart.php
});

//****************** Admin Pages *******************    
//add item page
$app->get('/additem', function() use($app){
    authorizeAdmin($app);//Accounts.php
    $header = getHeader();//Accounts.php
    $app->render('additem.html',array('header'=>$header, 
                                      'failure'=>'no items added yet'));
});
//add item form is posted -> store item in database
$app->post('/additem', function() use($app){
    authorizeAdmin($app);//Accounts.php
    $template_vars = post_addItem();//ItemManager.php
    $app->render('additem.html', $template_vars);
});
//list of customer orders to be filled:
$app->get('/orders', function() use($app){
    authorizeAdmin($app);//Accounts.php
    $template_vars = customerOrders();//Orders.php
    $app->render('orders.html', $template_vars);
});
//mark item shipped
$app->get('/shiporder', function() use($app){
    authorizeAdmin($app);//Accounts.php
    shipOrder($_GET['ordernumber']);//Orders.php
});
######################################################
$app->run();
?>