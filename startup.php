<?php
/*  --------------- steps to getting this running ------------------
*   This runs on the Slim framework: https://github.com/codeguy/Slim
*   and requires Slim-Extras: https://github.com/codeguy/Slim-Extras
*   as well as Twig templating engine https://github.com/fabpot/Twig
*   and finally the RedBean ORM: http://redbeanphp.com/
*
*   Docs:
*   //http://www.slimframework.com/
*   //http://twig.sensiolabs.org/
*
*   Directory Structure:
*   Slim is in /Slim
*   Slim-Extras is in /Slim/Extras
*   RedBean is in /Slim/RedBean
*   Twig is in /Slim/Twig
*      ^ is from "Twig-master.zip\Twig-master\lib\Twig"
*   templates are in /Templates
*
*   You must change /Slim/Extras/Views/Twig.php to match 'public static $twigDirectory = �Slim/Twig�;'
*/
require 'Slim/Slim.php'; Slim\Slim::registerAutoloader();
require 'Slim/Extras/Views/Twig.php';
require 'Slim/RedBean/rb.php';
require 'Stripe/Stripe.php';

use Slim\Slim as Slim;
use Slim\Extras\Views\Twig as Twig;

$app = new Slim(array(
    'view' => new Twig,
    'templates.path' => 'Templates'
));

session_start();
?>