$(document).ready(function(){
    
    //submit add item form
    //disable button to prevent double submissions
    $("#submititem").click(function(){
        $(this).prop('disabled', true);
        $("#additem").submit();
    });

    //mark item as shipped
    $(".shipped").click(function(){
        $(this).prop('disabled', true);
        $(this).hide();
        $(this).parent().css('color','#BBB');
        $("#additem").submit();
        var ordernumber = $(this).parent().find('.ordernumber').text();
        $.get("/shiporder",{ordernumber:ordernumber});
    });

});
