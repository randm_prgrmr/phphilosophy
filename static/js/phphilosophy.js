$(document).ready(function(){
    //cart and login slidedown panes hidden by default
    var login_hidden = true;
    var cart_hidden = true;
    var fetching_more_items = false;
    var cart_items = new Array();
    
    //INITIALIZE JS SHOPPING CART FROM CART STORED IN SESSION
    $.getJSON('/cart.json').done(function(data) {
        //data[0] is array of productids
        //data[1] is array of arrays, [[name, price],[name,price]]
        cart_items=data[0];//array of productids
        $("#cartcount").text(cart_items.length);
        for(var i=0; i < cart_items.length; i++){
            //for item in cart, add item element to headerbar slidedown cart
            $("#cartbox").append("<div class='cartitem'>"+
                "<span class='productid'>"+data[0][i]+"</span><br>"+
                "<span>"+data[1][i][0]+"</span><br>"+
                "<a href='#' id='removeitem'>Remove</a></div>"
            );
        }
        colorizePageItems();
    });
    
    function checkoutButtonUpdate(){
        //show or hide checkout button depending on whether cart is empty
        if(cart_items.length > 0){
            $("#checkout").css('display','inline-block');
            $("#carttext").hide();
        }
        else{
            $("#carttext").show();
            $("#checkout").hide();
        }    
    }
    
    function colorizePageItems(){
        /*iterate over items displayed on current page
        if they are in the shopping cart, set border color green
        else revert to default gray border color*/
        $('.item').each(function(){
            var productid = $(this).children('.productid').text();
            if($.inArray(productid,cart_items) != -1){
                //make item display div border green
                $(this).css('border','3px solid green');
                $(this).find('.addtocart').text('Remove');
            }
            else{            
                $(this).find('.addtocart').text('Add to cart');
                $(this).css('border','1px solid #AAA');
            }
        });
        checkoutButtonUpdate();
    }
    
    //ADD/REMOVE ITEM TO/FROM SHOPPING CART (page body only)
    function addToCart(item){
        //function takes a .item element as argument
        var productid = item.find('.productid').text();
        var itemname = item.find('.itemname').text();
        if($.inArray(productid, cart_items) == -1){
            //if the item isn't already in the cart, add it
            $("#cartbox").append("<div class='cartitem'>"+
                "<span class='productid'>"+productid+"</span><br>"+
                "<span>"+itemname+"</span><br>"+
                "<a href='#' id='removeitem'>Remove</a></div>"
            );
            cart_items.push(productid);
            var cartcount = parseInt($("#cartcount").text());
            $("#cartcount").text(cartcount+1);
            //add to php session shopping cart via ajax
            $.get("/addtocart",{productid:productid});
        }
        else{
            //if item already in cart, remove it from the cart
            var index = $.inArray(productid, cart_items);
            cart_items.splice(index,1);
            var cartcount = parseInt($("#cartcount").text());
            $("#cartcount").text(cartcount-1);
            $.get("/removefromcart",{productid:productid});
            $('.cartitem').each(function(){
                if($(this).find('.productid').text() == productid){$(this).remove();}
            });
    
        }
        colorizePageItems();

    }
    
    //REMOVE ITEM FROM SHOPPING CART (slidedown header cart)
    function removeItemByProductID(productid){
        //function takes productid string as argument
        $.get("/removefromcart",{productid:productid});
        var cartcount = parseInt($("#cartcount").text());
        $("#cartcount").text(cartcount-1);
        var index = $.inArray(productid, cart_items);
        cart_items.splice(index,1);
        colorizePageItems();
        
    }
//####################### ITEM DISPLAY #######################
    //DISPLAY PRICE ON MOUSEOVER OR CLICK(i.e. iPad has no mouseover)
    var SLIDESPEED = 200;
    
    $(document).on("mouseenter",".item",function(){
        $(this).find(".details").slideDown(SLIDESPEED);
    });
    
    //mouseleave preferrable to mouseout with nested image.
    $(document).on("mouseleave",".item",function(){
        $(this).find(".details").slideUp(SLIDESPEED);
    });
    $(document).on("click",".item",function(){
        $(this).find(".details").slideDown(SLIDESPEED);
    });
    
//############## SHOPPING CART  [header bar] #############
    //shopping cart button -> slide down shopping cart
    $("#slidecart").click(function(){
        if(cart_hidden){
            $('#cartpane').slideDown();
            cart_hidden = false;
        }
        else{
            $('#cartpane').slideUp();
            cart_hidden=true;
        }
    });
    
    //ADD/REMOVE SHOPPING CART ITEM
    //TODO: figure out why iOS doesn't see this
    $(document).on("click",".addtocart",function(){
        addToCart($(this).closest('.item'));
    });   
    
    $(document).on("click","#removeitem",function(){
        var productid = $(this).parent().find('.productid').text();
        $(this).parent().remove()
        removeItemByProductID(productid);
    });
    /*
    //ADD/REMOVE SHOPPING CART ITEM (iOS/Mobile)
    $('.addtocart').bind("touchstart",function(e){
        addToCart($(this).closest('.item'));
        });
    $("#removeitem").bind("touchstart",function(e){
        var productid = $(this).parent().find('.productid').text();
        $(this).parent().remove()
        removeItemByProductID(productid);
    });
    */
//######################### LOGIN / CREATE ACCOUNT #####################
    //SLIDE DOWN LOGIN PANEL
    $("#slidelogin").click(function(){
        if(login_hidden){
            $('#loginpane').slideDown();
            login_hidden = false;
        }
        else{
            $('#loginpane').slideUp();
            login_hidden=true;
        }
    });
    
    //SUBMIT NEW ACCOUNT FORM BY CLICKING BUTTON OR PRESSING ENTER ON PASSWORD
    $("#submitnewacct").click(function(){
        $(this).prop('disabled', true);
        $("#newacct").submit();
        }); 
    $('#newacct').keypress(function (e) {
        if (e.which == 13) {
            $("#newacct").submit();
            return false;//why?
        }
    });

    //SUBMIT LOGIN FORM BY CLICKING BUTTON OR PRESSING ENTER ON PASSWORD
    $("#submitlogin").click(function(){
        $(this).prop('disabled', true);
        $("#login").submit();
    });
    $('#login').keypress(function (e) {
        if (e.which == 13) {
            $("#login").submit();
            return false;//why?
        }
    });    
    
//############################## PAGINATION #############################
    //RETRIEVE NEXT PAGE OF ITEMS
    $("#more_items_button").click(function(){
        if(!fetching_more_items){//only look for more items if we're not already looking for more items
            fetching_more_items = true;
            //show the user we're looking for more items so they don't keep clicking when they don't have to
            $("#itemshelf").append("<h2 id='findingmore'>...finding more items...</h2>");
            $.get('/moreitems', function(data) {
                $("#findingmore").remove()//now that we've found items, hide the finding sign
                if(data.indexOf("no more products") == -1){
                    $('#itemshelf').append(data);
                    }
                else{
                    $("#itemshelf").append("<h2 id='findingmore'>That's all we have in stock right now.</h2>");
                    $('#more_items_button').hide();//we reached the end of the inventory, hide the next page button
                }
                fetching_more_items = false;
            }).done(function() {colorizePageItems();});
        }
    });
});

//##################### CHECKOUT & PAYMENT PROCESSING ######################
//Show "finalize and send" button when user clicks Continue on checkout page
$(document).on("click","#checkoutsubmit",function(){
    //$(this).css('visibility','hidden');//hide();
    $(this).hide();
    $("#finalizeorder").fadeIn();
});

//iOS/Mobile/Touch
$('#checkoutsubmit').bind("touchstart",function(e){
    $(this).hide();
    $("#finalizeorder").slideDown();
});