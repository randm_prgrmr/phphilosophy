================
  phphilosophy
================

PHP store/shopping cart/cms I'm writing for a friend's hobby project and to use as a code sample.

Should cover that I know: PHP, SQL, JavaScript, jQuery, HTML, CSS, Twig templating, RedBeanPHP ORM, and obviously Git.