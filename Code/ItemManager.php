<?php

function parsePrice($price){
    //convert price into cents, our DB & Stripe both use cents for prices.
    $price .= '.000';//add decimals in case user has not input any
    $price = str_replace('$', '', $price);
    $price = explode('.',$price);
    $price = $price[0]*100 + substr($price[1],0,2);//first two digits of cents only... otherwise $1.0000 = 10000 cents = $100
    return $price;
}

function saveImage($item){
    //validate & save product image to disk, filename is product's globally unique ID
    if($_FILES["file"]["error"] > 0){
        //echo "Error: " . $_FILES["file"]["error"] . "<br>";
        return false;
    }
    else{/*rename to .jpg, browser can figure it out for now.
           TODO: convert images to JPG if they're not already.
           TODO: limit filesize, discard non-image formats
           TODO: resize JPGs to 220px wide
           */
        $filename = $item->productid.'.jpg';
        move_uploaded_file($_FILES["file"]["tmp_name"],'static/productimages/'.$filename);
        return true;
    }
}

function constructItem(){
    //add new product to database, store its image on disk, return product object
    //TODO: more validation. RedBeanPHP already protects against injection (via PDO)
    $item = R::dispense('item');//RedBeanPHP ORM object (a 'Bean')
    $item->category= $_POST['category'];
    $item->name = $_POST['name'];
    $item->description = $_POST['description'];
    $item->price = parsePrice($_POST['price']);//convert price to cents for storage
    $item->productid = uniqid();//globally unique product id
    $item->sold = False; //Mark as sold to remove from store display
    
    //if price is 0, error out before saving anything
    if($item->price == 0){
        return 'noprice';
    }
    
    $image = saveImage($item);//save image to disk
    if(!$image){
        return 'noimage';
    }
    
    R::store($item);//add item to database
    return $item;
}

function post_addItem(){
    //receive form with item details, create and validate an item object, add it to database
    $item = constructItem();
    
    if($item == 'noprice'){
        $template_variables = array('failure'=>'You forgot to input a price.');
    }
    else if($item=='noimage'){
        $template_variables = array('failure'=>'You didn\'nt upload an image, or there was an image error');
    }
    else{
        $template_variables = array('failure'=>false, 'item'=>$item);//'name'=> $item->name,
                                   // 'price'=>$item->price);
    }
    $template_variables['header'] = getHeader();//Accounts.php
    return $template_variables;
}

function get_moreItems(){

}
?>