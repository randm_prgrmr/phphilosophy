<?php
function constructOrder(){
    //create order object and add order to database
    $order = R::dispense('order');
    $order->custname = $_POST['custname'];
    $order->street = $_POST['street'];
    $order->city = $_POST['city'];
    $order->state = $_POST['state'];
    $order->zip = $_POST['zip'];
    $order->ordernumber = uniqid();
    $order->filled = False;

    R::store($order);
    return $order;
}

function processOrder($order){
    //mark purchased items as "sold" so other customers can filter results, set 'ordernumber' to match order
    $orderitems = Array();
    $failed_items = Array();
    
    //fetch customer's items from the database and mark them as Sold
    //(checking first to see if they were already sold while the customer was shopping)
    foreach(array_keys($_SESSION['cart']) as $productid){
        $item = R::findOne('item',"productid = ?",Array($productid));
        if($item->sold){
            array_push($failed_items, $item);
        }
        else{
            $item->sold = True;
            $item->ordernumber = $order->ordernumber;
            array_push($orderitems,$item);
        }
    }
    
    /*if any items are already sold, e.g. bought by another customer before you checked out,
    cancel the order, delete the affected items from the customer's shopping cart, and show them an error.*/
    if(!empty($failed_items)){
        R::trash($order);//delete the order
        foreach($orderitems as $item){//iterate over items and delete sold items from shopping cart
            $cartitemindex = array_search($_SESSION['cart'],$item->productid);
            array_splice($_SESSION['cart'],$cartitemindex,1);
            }
    }
    
    //else mark all the customer's items as sold, bill them, and give them a receipt
    else{
        foreach($orderitems as $item){
            R::store($item);
        }
        

        
        
        
        
        unset($_SESSION['cart']);//purchase complete, empty their cart
    }   
    return $failed_items;
}

function post_Order(){
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here https://manage.stripe.com/account
    Stripe::setApiKey('sk_test_mkGsLqEW6SLnZa487HYfJVLf');

    // Get the credit card details submitted by the form
    $token = $_POST['stripeToken'];
    
    // Create the charge on Stripe's servers - this will charge the user's card
    try {
    $charge = Stripe_Charge::create(array(
      'amount' => 1000, // amount in cents, again
      'currency' => 'usd',
      'card' => $token,
      'description' => 'payinguser@example.com')
    );
    } catch(Stripe_CardError $e) {
      // The card has been declined
      return Array('error'=>'Payment processing error: '.$e->getMessage());
    }
            
    //check whether user's shopping cart is empty, if so, abort processing transaction
    if(count($_SESSION['cart'])){
        $order = constructOrder();
        $failed_items = processOrder($order);
        if(count($failed_items)){
            return Array('error'=>'items purchased','items'=>$failed_items);
        }

        return Array('error'=>false,'ordernumber'=>$order->ordernumber);
    }
    else{
        return Array('error'=>'Your shopping cart is empty');
    }
}

function customerOrders(){
    //return unfilled customer orders for shipping page.
    $orders = R::findAll('order',"WHERE filled <> 1 ORDER BY id ASC");
    $template_vars = Array('orders'=>$orders);
    $itemarray = Array();
    foreach($orders as $order){
        $ordernumber = $order->ordernumber;
        $orderitems = R::findAll('item',"WHERE ordernumber = ?",Array($ordernumber));
        $itemarray[$order->ordernumber] = $orderitems;
    }
    $template_vars['header'] = getHeader();
    $template_vars['itemarray']=$itemarray;
    return $template_vars;
}

function shipOrder($ordernumber){
//mark order as shipped to customer
    $order = R::findOne('order',"ordernumber = ?",Array($ordernumber));
    $order->filled = True;
    R::store($order);
}

?>