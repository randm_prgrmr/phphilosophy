<?php
if(!isset($_SESSION['cart'])){
    $_SESSION['cart'] = Array();
}

function addToCart($productid){
    //add item to session shopping cart by item's product ID
    if(!array_key_exists($productid,$_SESSION['cart'])){
        $item = R::findOne('item',"productid = ?",array($productid));
        $template_variables = array('cart'=>array($item));
        $_SESSION['cart'][$productid] = Array($item->name,$item->price);
    }
    else{
        $template_variables = array('cart'=>'');
    }
    return $template_variables;
}

function removeFromCart($productid){
    //remove item from session shopping cart by item's product id
    if(array_key_exists($productid,$_SESSION['cart'])){
        unset($_SESSION['cart'][$productid]);
    }
    $template_variables = array('error'=>'none');
    return  $template_variables;
}

function cartTotal(){
    //return shopping cart value in dollars
    $total = 0;
    foreach(array_keys($_SESSION['cart']) as $productid){
        $item = R::findOne('item',"productid = ?",Array($productid));
        $total += $item->price;
    }
    return $total;
}

function cartJSON(){
    //construct and print JSON object respresenting the shopping cart
    $response['Content-Type'] = 'application/json';
    $cart = Array(array_keys($_SESSION['cart']),array_values($_SESSION['cart']));
    echo json_encode($cart);
    exit();
}



?>