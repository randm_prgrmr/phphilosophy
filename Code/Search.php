<?php

function newestItems(){
    //retrieve 8 newest items to display on homepage
    $header = getHeader();
    $products = R::findAll('item','WHERE sold <> 1 ORDER BY id DESC LIMIT 8');
    $_SESSION['itemlistend'] = end($products)->id;
    reset($products);
    $template_variables = array('products'=>$products,'header'=>$header);
    return $template_variables;
}

function moreItems($N){
    //retrieve next N items to display on next page or infinite scroll
    $itemlistend = $_SESSION['itemlistend'];
    $header = getHeader();
    $products = R::findAll('item',"WHERE id < ? AND sold <> 1 ORDER BY id DESC LIMIT ?",Array($itemlistend,$N));
    $template_variables = array('products'=>$products,'header'=>$header);
    
    if(!empty($products)){
        $endid = end($products)->id;
        $_SESSION['itemlistend'] = $endid;
        reset($products);
    }
    else{
        die('no more products');
    }
    return $template_variables;
}

?>