<?php
function getHeader(){
    //check if user is logged in, if so return corresponding header, else return default header
    if(!isset($_SESSION['loggedin'])){
        $header = 'headers/defaultheader.html';    
    }
    else if($_SESSION['loggedin']=='admin'){
        $header = 'headers/adminheader.html';
    }
    else if($_SESSION['loggedin'] == 'user'){
        $header = 'headers/userheader.html';
    }
    return $header;
}

function authorizeAdmin($app){
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']=='admin'){
        return true;
    }
    $app->redirect('/');
}

function post_newAcct(){               
    if (R::findOne('acct',"email=?",array($_POST['email']))){
        $template_variables = array('error'=>true,
                                    'errormsg'=>'E-mail address already in use.');
    }

    else if(!($_POST['password']==$_POST['password2'])){
        $template_variables = array('error'=>'passwords don\'t match.');
    }
    else{
        $salt = base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM));
        
        $acct = R::dispense('acct');//RedBeanPHP ORM object (a 'Bean')
        $acct->email= $_POST['email'];
        $acct->password = crypt($_POST['password'],$salt);
        $acct->salt = $salt;
        $acct->usertype = 'user';
        
        R::store($acct);//save user to DB
        $_SESSION['loggedin'] = $acct->usertype;
        $template_variables = array('error'=>false,
                                    'errormsg'=>'SUCCESS',
                                    'salt'=>$salt,
                                    'password'=>$acct->password,
                                    'session'=>$_SESSION['loggedin']
                                    );
    }
    return $template_variables;
}

function post_login(){
    $acct = R::findOne('acct',"email=?",array($_POST['email']));
    $submitted_password = crypt($_POST['password'],$acct->salt);
    if ($acct->password == $submitted_password){
        $template_variables = array('error'=>false);
        $_SESSION['loggedin'] = $acct->usertype;
    }
    else{
        $template_variables = array('error'=>'wrong username or password');
    }
    return $template_variables;
}

function signOut(){
    session_destroy(); 
}

function signIn(){
    return array('examplevarb'=>'this is the home page');
}

?>